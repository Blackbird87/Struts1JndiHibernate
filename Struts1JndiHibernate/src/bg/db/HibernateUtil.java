package bg.db;

import java.util.logging.Level;
import java.util.logging.Logger;

import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

public class HibernateUtil {

	private final static Logger LOGGER = Logger.getLogger(HibernateUtil.class.getName());
	private static SessionFactory factory = null;
	private static Configuration conf;

	private static SessionFactory buildSessionFactory() {
		try {
			conf = new Configuration();
			conf.configure("bg/hello/hibernate-cfg.xml");
			System.out.println("Hibernate configuration load success");

			factory = conf.buildSessionFactory();
			System.out.println("Hibernate session factory created");

		} catch (Exception e) {
			LOGGER.log(Level.WARNING, e.toString());
		}
		return factory;
	}

	public static SessionFactory getSessionFactory() {
		
		if(factory == null)
		{
			factory = buildSessionFactory();
		}
		
		return factory;
	}

}
