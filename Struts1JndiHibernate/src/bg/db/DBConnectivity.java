package bg.db;

import java.util.logging.Level;
import java.util.logging.Logger;
import org.hibernate.Session;
import org.hibernate.Transaction;

import bg.forms.HelloForm;

public class DBConnectivity {
	private final static Logger LOGGER = Logger.getLogger(DBConnectivity.class.getName());

	public void addHelloForm(HelloForm helloForm) {

		try {

			Session session = HibernateUtil.getSessionFactory().getCurrentSession();
			Transaction transaction = session.beginTransaction();
			session.save(helloForm);
			System.out.println("Entity saved in DB " + helloForm.getFirstName());
			transaction.commit();
			session.close();

		} catch (Exception e) {
			LOGGER.log(Level.WARNING, e.toString());
		}

	}
	
	public void deletehelloForm(HelloForm helloForm) {

		try {

			Session session = HibernateUtil.getSessionFactory().getCurrentSession();
			Transaction transaction = session.beginTransaction();
			session.delete(helloForm);
			System.out.println("Entity deleted in DB " + helloForm.getFirstName());
			transaction.commit();
			session.close();

		} catch (Exception e) {
			LOGGER.log(Level.WARNING, e.toString());
		}

	}

}
