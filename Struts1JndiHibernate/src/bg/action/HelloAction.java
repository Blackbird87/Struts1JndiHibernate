package bg.action;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import bg.db.DBConnectivity;
import bg.forms.HelloForm;

public class HelloAction extends Action{
	

	@Override
	public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
	
		ActionForward forward = mapping.getInputForward();
		
		
		HelloForm helloForm = (HelloForm)form;
		helloForm.setFirstName("Kolio");
		helloForm.setLastName("Ivanov");
		
		DBConnectivity dbConnectivity = new DBConnectivity();
		dbConnectivity.addHelloForm(helloForm);
		
		System.out.println("DB insert completed successfully");
	
			forward = mapping.findForward("success");

		
		return forward ;
		
		
	}
	
	
}
